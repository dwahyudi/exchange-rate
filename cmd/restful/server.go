package main

import (
	"context"
	"exchange_rate/infra"
	"exchange_rate/restful"
	"exchange_rate/services/rateservice"
	"log"

	"github.com/robfig/cron/v3"
)

func main() {
	infra := infra.InjectNewInfra()

	runScheduler(infra)

	restful.InjectNewRestful(infra).Run()
}

// TODO: When moving to k8s, remove this and just use k8s cron.
func runScheduler(infra infra.Infra) {
	log.Println("Running scheduler...")
	c := cron.New()

	c.AddFunc("0 8 * * *", func() {
		log.Println("Populating currency rate.")

		rateService := rateservice.InjectNewRateService(infra)
		err := rateService.Populate(context.Background())
		if err != nil {
			log.Fatal(err)
		}
	})
	c.Start()
}
