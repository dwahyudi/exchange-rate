package main

import (
	"context"
	"database/sql"
	"exchange_rate/infra"
	"exchange_rate/services/rateservice"
	"log"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/pgx"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func main() {
	dbMigrate()
	populateData()
}

func dbMigrate() {
	log.Println("Running db migration (if any)...")

	infra := infra.InjectNewInfra()

	db, err := sql.Open("pgx", infra.Config().PG_WRITE_URL)
	if err != nil {
		log.Fatal(err)
	}

	driver, err := pgx.WithInstance(db, &pgx.Config{
		DatabaseName: infra.DB().Read.Config().Database,
		SchemaName:   "public",
	})
	if err != nil {
		log.Fatal(err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://repo/pg_migrations",
		"postgres", driver)
	if err != nil {
		log.Fatal(err)
	}
	m.Up()
}

func populateData() {
	log.Println("Populating data.")

	infra := infra.InjectNewInfra()

	populateCurrencyRate(infra)
}

func populateCurrencyRate(infra infra.Infra) {
	log.Println("Populating currency rate.")

	rateService := rateservice.InjectNewRateService(infra)
	err := rateService.Populate(context.Background())
	if err != nil {
		log.Fatal(err)
	}
}
