package rateservice

import (
	"context"
	"exchange_rate/entity"
	"exchange_rate/repo/raterepo"
	"exchange_rate/repo/ratesourcerepo"

	"github.com/shopspring/decimal"
)

type (
	RateService interface {
		Populate(ctx context.Context) error
		FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error)
		FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error)
		GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error)
	}

	rateService struct {
		rateSourceRepo ratesourcerepo.RateSourceRepo
		rateRepo       raterepo.RateRepo
	}
)

func (r *rateService) FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error) {
	exchangeRates, err := r.rateRepo.FindByDate(ctx, date)
	if err != nil {
		return nil, err
	}

	return exchangeRates, nil
}

func (r *rateService) FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error) {
	exchangeRates, err := r.rateRepo.FindLatestData(ctx)
	if err != nil {
		return nil, err
	}

	return exchangeRates, nil
}

func (r *rateService) Populate(ctx context.Context) error {
	dataSource, err := r.rateSourceRepo.GetData(ctx)
	if err != nil {
		return err
	}

	exchangeRates := make([]entity.ExchangeRate, 0)
	for _, timeCube := range dataSource.Cube.Cube {
		date := timeCube.Time

		for _, rateCube := range timeCube.Cube {
			rate, err := decimal.NewFromString(rateCube.Rate)
			if err != nil {
				return err
			}
			exchangeRates = append(exchangeRates, entity.ExchangeRate{
				Currency: rateCube.Currency,
				Rate:     rate,
				Date:     date,
			})
		}
	}

	err = r.rateRepo.BatchInsert(ctx, exchangeRates)
	if err != nil {
		return err
	}

	return nil
}

func (r *rateService) GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error) {
	analysis, err := r.rateRepo.GetAnalysis(ctx)
	if err != nil {
		return nil, err
	}

	return analysis, nil
}
