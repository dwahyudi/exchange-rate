package rateservice

import (
	"context"
	"exchange_rate/entity"
	"exchange_rate/repo/mocks"
	"exchange_rate/repo/raterepo"
	"exchange_rate/repo/ratesourcerepo"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

var (
	rateRepoMock       = new(mocks.RateRepoMock)
	rateSourceRepoMock = new(mocks.RateSourceRepoMock)
)

func Test_rateService_GetAnalysis(t *testing.T) {
	type fields struct {
		rateSourceRepo ratesourcerepo.RateSourceRepo
		rateRepo       raterepo.RateRepo
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		before  func()
		want    []entity.RateAnalysis
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				rateRepo:       rateRepoMock,
				rateSourceRepo: rateSourceRepoMock,
			},
			before: func() {
				rateRepoMock.On("GetAnalysis").Return(
					[]entity.RateAnalysis{
						{
							Currency: "IDR",
							Min:      decimal.NewFromInt(14000),
							Max:      decimal.NewFromInt(12000),
							Avg:      decimal.NewFromInt(18000),
						},
					}, nil,
				)
			},
			args: args{
				ctx: context.Background(),
			},
			want: []entity.RateAnalysis{
				{
					Currency: "IDR",
					Min:      decimal.NewFromInt(14000),
					Max:      decimal.NewFromInt(12000),
					Avg:      decimal.NewFromInt(18000),
				},
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			r := &rateService{
				rateSourceRepo: tt.fields.rateSourceRepo,
				rateRepo:       tt.fields.rateRepo,
			}
			got, err := r.GetAnalysis(tt.args.ctx)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_rateService_FindLatestData(t *testing.T) {
	type fields struct {
		rateSourceRepo ratesourcerepo.RateSourceRepo
		rateRepo       raterepo.RateRepo
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		before  func()
		want    []entity.ExchangeRate
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				rateRepo:       rateRepoMock,
				rateSourceRepo: rateSourceRepoMock,
			},
			args: args{
				ctx: context.Background(),
			},
			before: func() {
				rateRepoMock.On("FindLatestData").Return(
					[]entity.ExchangeRate{
						{
							Currency: "IDR",
							Rate:     decimal.NewFromInt(16000),
							Date:     "2040-01-01",
						},
					}, nil,
				)
			},
			wantErr: nil,
			want: []entity.ExchangeRate{
				{
					Currency: "IDR",
					Rate:     decimal.NewFromInt(16000),
					Date:     "2040-01-01",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			r := &rateService{
				rateSourceRepo: tt.fields.rateSourceRepo,
				rateRepo:       tt.fields.rateRepo,
			}
			got, err := r.FindLatestData(tt.args.ctx)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_rateService_FindByDate(t *testing.T) {
	type fields struct {
		rateSourceRepo ratesourcerepo.RateSourceRepo
		rateRepo       raterepo.RateRepo
	}
	type args struct {
		ctx  context.Context
		date string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		before  func()
		want    []entity.ExchangeRate
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				rateRepo:       rateRepoMock,
				rateSourceRepo: rateSourceRepoMock,
			},
			args: args{
				ctx:  context.Background(),
				date: "2040-01-01",
			},
			before: func() {
				rateRepoMock.On("FindByDate", "2040-01-01").Return(
					[]entity.ExchangeRate{
						{
							Currency: "IDR",
							Rate:     decimal.NewFromInt(16000),
							Date:     "2040-01-01",
						},
					}, nil,
				)
			},
			wantErr: nil,
			want: []entity.ExchangeRate{
				{
					Currency: "IDR",
					Rate:     decimal.NewFromInt(16000),
					Date:     "2040-01-01",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			r := &rateService{
				rateSourceRepo: tt.fields.rateSourceRepo,
				rateRepo:       tt.fields.rateRepo,
			}
			got, err := r.FindByDate(tt.args.ctx, tt.args.date)
			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}

func Test_rateService_Populate(t *testing.T) {
	type fields struct {
		rateSourceRepo ratesourcerepo.RateSourceRepo
		rateRepo       raterepo.RateRepo
	}
	type args struct {
		ctx context.Context
	}

	sampleRate, _ := decimal.NewFromString("16500")
	sampleRate2, _ := decimal.NewFromString("0.88")

	tests := []struct {
		name    string
		fields  fields
		args    args
		before  func()
		wantErr error
	}{
		{
			name: "success",
			fields: fields{
				rateRepo:       rateRepoMock,
				rateSourceRepo: rateSourceRepoMock,
			},
			args: args{
				ctx: context.Background(),
			},
			before: func() {
				rateSourceRepoMock.On("GetData").Return(
					entity.EuropaDataSource{
						Cube: struct {
							Text string "xml:\",chardata\""
							Cube []struct {
								Text string "xml:\",chardata\""
								Time string "xml:\"time,attr\""
								Cube []struct {
									Text     string "xml:\",chardata\""
									Currency string "xml:\"currency,attr\""
									Rate     string "xml:\"rate,attr\""
								} "xml:\"Cube\""
							} "xml:\"Cube\""
						}{Cube: []struct {
							Text string "xml:\",chardata\""
							Time string "xml:\"time,attr\""
							Cube []struct {
								Text     string "xml:\",chardata\""
								Currency string "xml:\"currency,attr\""
								Rate     string "xml:\"rate,attr\""
							} "xml:\"Cube\""
						}{
							{
								Time: "2022-12-14",
								Cube: []struct {
									Text     string "xml:\",chardata\""
									Currency string "xml:\"currency,attr\""
									Rate     string "xml:\"rate,attr\""
								}{
									{
										Currency: "IDR",
										Rate:     "16500",
									},
									{
										Currency: "USD",
										Rate:     "0.88",
									},
								},
							},
						}},
					}, nil,
				)

				rateRepoMock.On("BatchInsert", []entity.ExchangeRate{
					{
						Date:     "2022-12-14",
						Currency: "IDR",
						Rate:     sampleRate,
					},
					{
						Date:     "2022-12-14",
						Currency: "USD",
						Rate:     sampleRate2,
					},
				}).Return(nil)
			},
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.before()
			r := &rateService{
				rateSourceRepo: tt.fields.rateSourceRepo,
				rateRepo:       tt.fields.rateRepo,
			}
			err := r.Populate(tt.args.ctx)
			assert.Equal(t, tt.wantErr, err)
		})
	}
}
