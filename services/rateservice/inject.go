package rateservice

import (
	"exchange_rate/infra"
	"exchange_rate/repo/raterepo"
	"exchange_rate/repo/ratesourcerepo"
	"sync"
)

var (
	injRateServiceOnce sync.Once
	injRateService     RateService
)

func InjectNewRateService(infra infra.Infra) RateService {
	injRateServiceOnce.Do(func() {
		injRateService = &rateService{
			rateSourceRepo: ratesourcerepo.InjectNewRatePopulatorRepo(infra),
			rateRepo:       raterepo.InjectNewRateRepo(infra),
		}
	})
	return injRateService
}
