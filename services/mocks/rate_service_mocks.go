package mocks

import (
	"context"
	"exchange_rate/entity"

	"github.com/stretchr/testify/mock"
)

type RateServiceMock struct {
	mock.Mock
}

func (rsm *RateServiceMock) Populate(ctx context.Context) error {
	panic("not implemented") // TODO: Implement
}

func (rsm *RateServiceMock) FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error) {
	args := rsm.Called()
	return args.Get(0).([]entity.ExchangeRate), args.Error(1)
}

func (rsm *RateServiceMock) FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error) {
	args := rsm.Called(date)
	return args.Get(0).([]entity.ExchangeRate), args.Error(1)
}

func (rsm *RateServiceMock) GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error) {
	args := rsm.Called()
	return args.Get(0).([]entity.RateAnalysis), args.Error(1)
}
