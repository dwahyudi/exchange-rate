package infra

import (
	"exchange_rate/config"
	"sync"
)

var (
	injInfraOnce sync.Once
	injInfra     Infra
)

func InjectNewInfra() Infra {
	injInfraOnce.Do(func() {
		injInfra = &infra{
			config: config.ReadConfig(),
		}
	})
	return injInfra
}
