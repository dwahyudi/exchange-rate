package infra

import (
	"context"
	"exchange_rate/config"
	"log"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type (
	Infra interface {
		DB() DB
		Config() config.Config
	}

	infra struct {
		config config.Config
	}

	DB struct {
		Read, Write *pgx.Conn
	}
)

func (i *infra) DB() DB {
	readCfg, err := pgxpool.ParseConfig(i.config.PG_READ_URL)
	if err != nil {
		log.Fatal(err)
	}
	readDB, err := pgx.ConnectConfig(context.Background(), readCfg.ConnConfig)
	if err != nil {
		log.Fatal(err)
	}

	writeCfg, err := pgxpool.ParseConfig(i.config.PG_WRITE_URL)
	if err != nil {
		log.Fatal(err)
	}
	writeDB, err := pgx.ConnectConfig(context.Background(), writeCfg.ConnConfig)
	if err != nil {
		log.Fatal(err)
	}

	return DB{Read: readDB, Write: writeDB}
}

func (i *infra) Config() config.Config {
	return i.config
}
