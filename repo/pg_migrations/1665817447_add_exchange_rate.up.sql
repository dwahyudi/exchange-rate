BEGIN;

CREATE TABLE exchange_rate (
	id bigserial NOT NULL primary key,
	currency varchar NOT NULL,
	rate decimal(12, 2) NOT NULL,
	"date" date NOT NULL
);
CREATE INDEX exchange_rate_date_idx ON exchange_rate ("date");
CREATE INDEX exchange_rate_currency_idx ON exchange_rate (currency);
CREATE UNIQUE INDEX exchange_rate_date_currency_unq_idx ON exchange_rate("date", currency);

COMMIT;