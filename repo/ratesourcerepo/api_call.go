package ratesourcerepo

import (
	"context"
	"encoding/xml"
	"exchange_rate/entity"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml"
)

type (
	apiCall struct {
	}
)

func (a *apiCall) GetData(ctx context.Context) (entity.EuropaDataSource, error) {
	var httpClient = &http.Client{
		Timeout: time.Second * 30,
	}

	var result entity.EuropaDataSource

	resp, err := httpClient.Get(url)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return result, err
		}
		return result, fmt.Errorf("error API call: %v", data)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	err = xml.Unmarshal(data, &result)
	if err != nil {
		return result, err
	}

	return result, nil
}
