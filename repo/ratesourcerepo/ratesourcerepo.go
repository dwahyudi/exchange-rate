package ratesourcerepo

import (
	"context"
	"exchange_rate/entity"
)

type (
	RateSourceRepo interface {
		GetData(ctx context.Context) (entity.EuropaDataSource, error)
	}
)
