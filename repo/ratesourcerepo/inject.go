package ratesourcerepo

import (
	"exchange_rate/infra"
	"sync"
)

var (
	injRatePopulatorRepoOnce sync.Once
	injRatePopulator         RateSourceRepo
)

func InjectNewRatePopulatorRepo(infra infra.Infra) RateSourceRepo {
	injRatePopulatorRepoOnce.Do(func() {
		injRatePopulator = &apiCall{}
	})
	return injRatePopulator
}
