package raterepo

import (
	"exchange_rate/infra"
	"sync"
)

var (
	injRateRepoOnce sync.Once
	injRateRepo     RateRepo
)

func InjectNewRateRepo(infra infra.Infra) RateRepo {
	injRateRepoOnce.Do(func() {
		injRateRepo = &pgRepo{infra: infra}
	})
	return injRateRepo
}
