package raterepo

import (
	"context"
	"exchange_rate/entity"
)

type (
	RateRepo interface {
		FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error)
		FindLatestDate(ctx context.Context) (string, error)
		FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error)
		GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error)
		BatchInsert(ctx context.Context, exchangeRates []entity.ExchangeRate) error
	}
)
