package raterepo

var (
	findByDate = `
		SELECT
		    currency,
		    rate
		FROM
		    exchange_rate
		WHERE
		    date = $1::DATE
		ORDER BY
		    currency`

	findLatestdate = `
		SELECT
		    max(date)
		FROM
		    exchange_rate`

	insert = `
		INSERT INTO exchange_rate (currency, rate, date)
		    VALUES ($1, $2, $3)
		ON CONFLICT (currency, date)
		    DO NOTHING`

	analyze = `
		SELECT
		    currency,
		    min(rate),
		    max(rate),
		    avg(rate)
		FROM
		    exchange_rate
		GROUP BY
		    currency
		ORDER BY
		    currency;
		`
)
