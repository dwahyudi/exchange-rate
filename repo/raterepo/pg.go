package raterepo

import (
	"context"
	"exchange_rate/entity"
	"exchange_rate/infra"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/shopspring/decimal"
)

type (
	pgRepo struct {
		infra infra.Infra
	}
)

func (pg *pgRepo) FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error) {
	rows, err := pg.infra.DB().Read.Query(ctx, findByDate, date)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	exchangeRates := make([]entity.ExchangeRate, 0)
	var (
		currency string
		rate     decimal.Decimal
	)
	for rows.Next() {
		err := rows.Scan(&currency, &rate)
		if err != nil {
			return nil, err
		}

		exchangeRates = append(exchangeRates, entity.ExchangeRate{
			Currency: currency,
			Rate:     rate,
			Date:     date,
		})
	}

	return exchangeRates, nil
}

func (pg *pgRepo) GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error) {
	rows, err := pg.infra.DB().Read.Query(ctx, analyze)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	analysis := make([]entity.RateAnalysis, 0)
	var (
		currency string
		min      decimal.Decimal
		max      decimal.Decimal
		avg      decimal.Decimal
	)
	for rows.Next() {
		err := rows.Scan(&currency, &min, &max, &avg)
		if err != nil {
			return nil, err
		}

		analysis = append(analysis, entity.RateAnalysis{
			Currency: currency,
			Min:      min,
			Max:      max,
			Avg:      avg,
		})
	}

	return analysis, nil
}

func (pg *pgRepo) FindLatestDate(ctx context.Context) (string, error) {
	var latestDate time.Time

	row := pg.infra.DB().Read.QueryRow(ctx, findLatestdate)
	err := row.Scan(&latestDate)
	if err != nil {
		return "", err
	}

	return latestDate.Format("2006-01-02"), nil
}

func (pg *pgRepo) FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error) {
	latestDate, err := pg.FindLatestDate(ctx)
	if err != nil {
		return nil, err
	}

	exchangeRates, err := pg.FindByDate(ctx, latestDate)
	if err != nil {
		return nil, err
	}

	return exchangeRates, nil
}

func (pg *pgRepo) BatchInsert(ctx context.Context, exchangeRates []entity.ExchangeRate) error {
	batch := &pgx.Batch{}

	for _, exchangeRate := range exchangeRates {
		batch.Queue(insert, exchangeRate.Currency, exchangeRate.Rate, exchangeRate.Date)
	}

	br := pg.infra.DB().Write.SendBatch(context.Background(), batch)
	defer br.Close()

	return nil
}
