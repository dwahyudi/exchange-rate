package mocks

import (
	"context"
	"exchange_rate/entity"

	"github.com/stretchr/testify/mock"
)

type RateRepoMock struct {
	mock.Mock
}

func (rrm *RateRepoMock) FindByDate(ctx context.Context, date string) ([]entity.ExchangeRate, error) {
	args := rrm.Called(date)
	return args.Get(0).([]entity.ExchangeRate), args.Error(1)
}

func (rrm *RateRepoMock) FindLatestDate(ctx context.Context) (string, error) {
	panic("not implemented") // TODO: Implement
}

func (rrm *RateRepoMock) FindLatestData(ctx context.Context) ([]entity.ExchangeRate, error) {
	args := rrm.Called()
	return args.Get(0).([]entity.ExchangeRate), args.Error(1)
}

func (rrm *RateRepoMock) GetAnalysis(ctx context.Context) ([]entity.RateAnalysis, error) {
	args := rrm.Called()
	return args.Get(0).([]entity.RateAnalysis), args.Error(1)
}

func (rrm *RateRepoMock) BatchInsert(ctx context.Context, exchangeRates []entity.ExchangeRate) error {
	args := rrm.Called(exchangeRates)
	return args.Error(0)
}
