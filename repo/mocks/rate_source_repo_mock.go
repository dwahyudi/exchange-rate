package mocks

import (
	"context"
	"exchange_rate/entity"

	"github.com/stretchr/testify/mock"
)

type RateSourceRepoMock struct {
	mock.Mock
}

func (rsrm *RateSourceRepoMock) GetData(ctx context.Context) (entity.EuropaDataSource, error) {
	args := rsrm.Called()
	return args.Get(0).(entity.EuropaDataSource), args.Error(1)
}
