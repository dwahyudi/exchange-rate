# Overview

A simple exchange rate service.

Source of data: https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml

# How to Run

* Run `cp .env_sample .env`. This is the secrets/environment variables that will be used by the app.
* Install docker compose: https://docs.docker.com/compose/install/
* Run `docker-compose build` to build this project.
* Run `docker-compose up` to run this project.

# Components

Check `docker-compose.yml` for more informations. List of services:

* `exchange_rate_app` the main Go app. This is the one who will serve the http requests from users/clients. It runs on `restful.dockerfile`.
  - Exposes port 8080 (for http server).
  - Run a scheduler to update exchange rate data every 8 AM.
* `exchange_rate_app_startup` a Go app that will be run as startup tasks, database migration and populating of currency rates will be done here. It runs on `startup.dockerfile`. Check `cmd/startup/startup.go` for the entrypoint of this task.
* `postgresqldb` is PostgreSQL database used by this project.
  - It has a volume mapping to `$HOME/docker-volumes/exchange-rate-postgres`.

# Structures

* `cmd` contains Go main files.
* `config` is a utility package for reading from environment variables, used by `infra`.
* `entity` is reusable structs for business logic.
* `infra` contains everything related to infrastructure (db connectivity, caching, configurations, etc).
* `repo` contains code related to data persistence.
  - `pg_migrations` contains sql files for database migration.
* `restful` contains http handlers and routings.
* `services` contains reusable services code.

# Dependencies

* `github.com/golang-migrate/migrate/v4` is a database migration library.
* `github.com/jackc/pgx/v5` is a postgresql driver, replacement for `pq`.
* `github.com/shopspring/decimal` is a library that provides correct handling of decimal data type.
* `github.com/go-chi/chi/v5` is a restful routing library.
* `github.com/robfig/cron/v3` for running scheduled tasks.
* `github.com/stretchr/testify` for unit tests + mocking tool.

# Available API

Check `restful/restful.go` for more informations. List of API:

* `curl localhost:8080/rates/`
* `curl localhost:8080/rates/2022-09-26`
* `curl localhost:8080/rates/analyze`

# Unit Tests

Make sure at least Go 1.14 is installed. Run this command:

```
go test ./... -v -coverpkg=./... -shuffle=on -cover -count=1
```