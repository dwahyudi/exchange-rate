package restfulcommon

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func SetResponse(w http.ResponseWriter, status int, message string) {
	w.WriteHeader(status)
	fmt.Fprintf(w, message)
}

func SetJSONResponse(w http.ResponseWriter, status int, message any) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)

	messageResponse, _ := json.Marshal(message)

	fmt.Fprint(w, string(messageResponse))
}
