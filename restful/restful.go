package restful

import (
	"exchange_rate/infra"
	"exchange_rate/restful/raterestful"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type (
	Restful interface {
		Run()
	}

	restful struct {
		infra infra.Infra
	}
)

func (rs *restful) Run() {
	var (
		r     = chi.NewRouter()
		infra = rs.infra

		rateRestful = raterestful.InjectNewRateRestful(infra)
	)

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.StripSlashes)

	r.Get("/rates/latest", rateRestful.FindLatestData)
	r.Get(`/rates/{date:\d\d\d\d-\d\d-\d\d}`, rateRestful.FindByDate)
	r.Get("/rates/analyze", rateRestful.Analyze)
	r.Get("/rates/populate", rateRestful.Populate)

	log.Println("Starting restful server at port 8080...")
	http.ListenAndServe(":8080", r)
}
