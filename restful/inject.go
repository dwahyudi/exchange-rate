package restful

import (
	"exchange_rate/infra"
	"sync"
)

var (
	injRestfulOnce sync.Once
	injRestful     Restful
)

func InjectNewRestful(infra infra.Infra) Restful {
	injRestfulOnce.Do(func() {
		injRestful = &restful{
			infra: infra,
		}
	})
	return injRestful
}
