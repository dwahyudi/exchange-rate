package raterestful

import (
	"exchange_rate/entity"
	"exchange_rate/services/rateservice"
	"net/http"

	rc "exchange_rate/restful/restfulcommon"

	"github.com/go-chi/chi/v5"
)

type (
	RateRestful interface {
		FindLatestData(w http.ResponseWriter, r *http.Request)
		FindByDate(w http.ResponseWriter, r *http.Request)
		Analyze(w http.ResponseWriter, r *http.Request)
		Populate(w http.ResponseWriter, r *http.Request)
	}

	rateRestful struct {
		rateService rateservice.RateService
	}
)

func (rr *rateRestful) FindLatestData(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	exchangeRates, err := rr.rateService.FindLatestData(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusInternalServerError, "Unknown error")
		return
	}

	exchangeRatesResponse := entity.ExchangeRateResponse{
		Base:          "EUR",
		ExchangeRates: exchangeRates,
	}

	rc.SetJSONResponse(w, http.StatusOK, exchangeRatesResponse)
}

func (rr *rateRestful) FindByDate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	date := chi.URLParam(r, "date")
	exchangeRates, err := rr.rateService.FindByDate(ctx, date)
	if err != nil {
		rc.SetResponse(w, http.StatusInternalServerError, "Unknown error")
		return
	}

	exchangeRatesResponse := entity.ExchangeRateResponse{
		Base:          "EUR",
		ExchangeRates: exchangeRates,
	}

	rc.SetJSONResponse(w, http.StatusOK, exchangeRatesResponse)
}

func (rr *rateRestful) Analyze(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	analysis, err := rr.rateService.GetAnalysis(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusInternalServerError, "Unknown error")
		return
	}

	analysisResponse := entity.AnalysisResponse{
		Base:         "EUR",
		RatesAnalyze: analysis,
	}

	rc.SetJSONResponse(w, http.StatusOK, analysisResponse)
}

func (rr *rateRestful) Populate(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	err := rr.rateService.Populate(ctx)
	if err != nil {
		rc.SetResponse(w, http.StatusInternalServerError, "Unknown error")
		return
	}

	rc.SetResponse(w, http.StatusOK, "ok")
}
