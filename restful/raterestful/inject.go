package raterestful

import (
	"exchange_rate/infra"
	"exchange_rate/services/rateservice"
	"sync"
)

var (
	injRateRestfulOnce sync.Once
	injRateRestful     RateRestful
)

func InjectNewRateRestful(infra infra.Infra) RateRestful {
	injRateRestfulOnce.Do(func() {
		injRateRestful = &rateRestful{
			rateService: rateservice.InjectNewRateService(infra),
		}
	})
	return injRateRestful
}
