package raterestful

import (
	"context"
	"exchange_rate/entity"
	"exchange_rate/services/mocks"
	"exchange_rate/services/rateservice"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

var (
	rateServiceMock = new(mocks.RateServiceMock)
)

func Test_rateRestful_FindLatestData(t *testing.T) {
	type fields struct {
		rateService rateservice.RateService
	}
	tests := []struct {
		name         string
		fields       fields
		before       func()
		responseCode int
		responseBody string
	}{
		{
			name:   "success",
			fields: fields{rateService: rateServiceMock},
			before: func() {
				rateServiceMock.On("FindLatestData").Return(
					[]entity.ExchangeRate{
						{
							Currency: "IDR",
							Rate:     decimal.NewFromInt(16000),
							Date:     "2040-01-01",
						},
					}, nil,
				)
			},
			responseCode: http.StatusOK,
			responseBody: `{"base":"EUR","rates":[{"currency":"IDR","rate":"16000","date":"2040-01-01"}]}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/rates/latest", nil)
			if err != nil {
				t.Fatal(err)
			}
			rec := httptest.NewRecorder()

			tt.before()
			rr := &rateRestful{
				rateService: tt.fields.rateService,
			}

			handler := http.HandlerFunc(rr.FindLatestData)
			handler.ServeHTTP(rec, req)

			assert.Equal(t, tt.responseBody, rec.Body.String())
			assert.Equal(t, tt.responseCode, rec.Code)
		})
	}
}

func Test_rateRestful_FindByDate(t *testing.T) {
	type fields struct {
		rateService rateservice.RateService
	}
	tests := []struct {
		name         string
		fields       fields
		before       func()
		responseCode int
		responseBody string
	}{
		{
			name:   "success",
			fields: fields{rateService: rateServiceMock},
			before: func() {
				rateServiceMock.On("FindByDate", "2022-09-26").Return(
					[]entity.ExchangeRate{
						{
							Currency: "IDR",
							Rate:     decimal.NewFromInt(16000),
							Date:     "2022-09-26",
						},
					}, nil,
				)
			},
			responseCode: http.StatusOK,
			responseBody: `{"base":"EUR","rates":[{"currency":"IDR","rate":"16000","date":"2022-09-26"}]}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/rates/2022-09-26", nil)
			if err != nil {
				t.Fatal(err)
			}
			rec := httptest.NewRecorder()
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("date", "2022-09-26")
			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

			tt.before()
			rr := &rateRestful{
				rateService: tt.fields.rateService,
			}

			handler := http.HandlerFunc(rr.FindByDate)
			handler.ServeHTTP(rec, req)

			assert.Equal(t, tt.responseBody, rec.Body.String())
			assert.Equal(t, tt.responseCode, rec.Code)
		})
	}
}

func Test_rateRestful_Analyze(t *testing.T) {
	type fields struct {
		rateService rateservice.RateService
	}
	tests := []struct {
		name         string
		fields       fields
		before       func()
		responseCode int
		responseBody string
	}{
		{
			name:   "success",
			fields: fields{rateService: rateServiceMock},
			before: func() {
				rateServiceMock.On("GetAnalysis").Return(
					[]entity.RateAnalysis{
						{
							Currency: "IDR",
							Min:      decimal.NewFromFloat(14000),
							Max:      decimal.NewFromFloat(18000),
							Avg:      decimal.NewFromFloat(18000),
						},
					}, nil,
				)
			},
			responseCode: http.StatusOK,
			responseBody: `{"base":"EUR","rates_analyze":[{"currency":"IDR","min":"14000","max":"18000","avg":"18000"}]}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", "/rates/2022-09-26", nil)
			if err != nil {
				t.Fatal(err)
			}
			rec := httptest.NewRecorder()
			rctx := chi.NewRouteContext()
			rctx.URLParams.Add("date", "2022-09-26")
			req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

			tt.before()
			rr := &rateRestful{
				rateService: tt.fields.rateService,
			}

			handler := http.HandlerFunc(rr.Analyze)
			handler.ServeHTTP(rec, req)

			assert.Equal(t, tt.responseBody, rec.Body.String())
			assert.Equal(t, tt.responseCode, rec.Code)
		})
	}
}
