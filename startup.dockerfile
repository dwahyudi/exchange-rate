FROM golang:1.19-alpine AS build-env
RUN apk --no-cache add ca-certificates
ENV APP_PATH="/app"
WORKDIR ${APP_PATH}
COPY go.mod ${APP_PATH}
COPY go.sum ${APP_PATH}
RUN go mod download
RUN go mod verify
COPY . ${APP_PATH}
COPY .env .
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" cmd/startup/startup.go

FROM scratch
COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENV APP_PATH="/app"
WORKDIR ${APP_PATH}
COPY --from=build-env ${APP_PATH} .
CMD ["./startup"]