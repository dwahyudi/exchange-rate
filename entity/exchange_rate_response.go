package entity

type (
	ExchangeRateResponse struct {
		Base          string         `json:"base"`
		ExchangeRates []ExchangeRate `json:"rates"`
	}

	AnalysisResponse struct {
		Base         string         `json:"base"`
		RatesAnalyze []RateAnalysis `json:"rates_analyze"`
	}
)
