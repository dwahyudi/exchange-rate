package entity

import (
	"encoding/xml"

	"github.com/shopspring/decimal"
)

type (
	ExchangeRate struct {
		Currency string          `json:"currency"`
		Rate     decimal.Decimal `json:"rate"`
		Date     string          `json:"date"`
	}

	RateAnalysis struct {
		Currency string          `json:"currency"`
		Min      decimal.Decimal `json:"min"`
		Max      decimal.Decimal `json:"max"`
		Avg      decimal.Decimal `json:"avg"`
	}

	EuropaDataSource struct {
		XMLName xml.Name `xml:"Envelope"`
		Text    string   `xml:",chardata"`
		Gesmes  string   `xml:"gesmes,attr"`
		Xmlns   string   `xml:"xmlns,attr"`
		Subject string   `xml:"subject"`
		Sender  struct {
			Text string `xml:",chardata"`
			Name string `xml:"name"`
		} `xml:"Sender"`
		Cube struct {
			Text string `xml:",chardata"`
			Cube []struct {
				Text string `xml:",chardata"`
				Time string `xml:"time,attr"`
				Cube []struct {
					Text     string `xml:",chardata"`
					Currency string `xml:"currency,attr"`
					Rate     string `xml:"rate,attr"`
				} `xml:"Cube"`
			} `xml:"Cube"`
		} `xml:"Cube"`
	}
)
