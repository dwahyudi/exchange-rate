package config

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

type (
	Config struct {
		PG_READ_URL            string
		PG_READ_MAX_OPEN_CONNS int
		PG_READ_MAX_IDLE_CONNS int

		PG_WRITE_URL            string
		PG_WRITE_MAX_OPEN_CONNS int
		PG_WRITE_MAX_IDLE_CONNS int
	}
)

func ReadConfig() Config {
	readMaxOpenConns, err := strconv.Atoi(os.Getenv("PG_READ_MAX_OPEN_CONNS"))
	if err != nil {
		log.Fatal(err)
	}
	readMaxIdleConns, err := strconv.Atoi(os.Getenv("PG_READ_MAX_IDLE_CONNS"))
	if err != nil {
		log.Fatal(err)
	}
	writeMaxOpenConns, err := strconv.Atoi(os.Getenv("PG_WRITE_MAX_OPEN_CONNS"))
	if err != nil {
		log.Fatal(err)
	}
	writeMaxIdleConns, err := strconv.Atoi(os.Getenv("PG_WRITE_MAX_IDLE_CONNS"))
	if err != nil {
		log.Fatal(err)
	}

	return Config{
		PG_READ_URL: fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s",
			os.Getenv("PG_READ_USERNAME"),
			os.Getenv("PG_READ_PASSWORD"),
			os.Getenv("PG_READ_HOST"),
			os.Getenv("PG_READ_PORT"),
			os.Getenv("PG_READ_DATABASE"),
		),
		PG_READ_MAX_OPEN_CONNS: readMaxOpenConns,
		PG_READ_MAX_IDLE_CONNS: readMaxIdleConns,
		PG_WRITE_URL: fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s",
			os.Getenv("PG_WRITE_USERNAME"),
			os.Getenv("PG_WRITE_PASSWORD"),
			os.Getenv("PG_WRITE_HOST"),
			os.Getenv("PG_WRITE_PORT"),
			os.Getenv("PG_WRITE_DATABASE"),
		),
		PG_WRITE_MAX_OPEN_CONNS: writeMaxOpenConns,
		PG_WRITE_MAX_IDLE_CONNS: writeMaxIdleConns,
	}
}
